#***********************************************************************************
# LIBRARIES
#***********************************************************************************
#source("./build_network.R")

#***********************************************************************************
# FILES
#***********************************************************************************
#tsv.file1 <- "../data/HI2_2011.tsv"
#data.file1 <- "../data/filtered.tsv"
#categorie.file <- "../data//categories//categoriesC5.tsv"
#subnets.file <- "../data/greedy_new//submat//subnets_k10"
#subnets.dir <- "../data/greedy_new//submat"

#out.d <- "../data/greedy_new//submat//genes_2_annotate"
#in.dir <- "../data/greedy_new//submat//subnets"
#*********************************************************************************
# BUILD NETWORK
#*********************************************************************************
#G <- build.graph.theorie(tsv.file1)
#final.G <- build.final.network(G,data.file1)

#*********************************************************************************
# FUNCTIONS
#*********************************************************************************
get.genes <- function(G,dir, out.dir){
  #if(k > 2) G <- pre.graph(G,k)
  #names = c("k5","k6","k7","k8","k9","k10","k11","k12","k13","k14","k15","k16","k17","k18","k19","k20","k21","k22","k23","k24","k25","k26","k27","k28","k29","k30")
  #res.mat <- matrix(0,ncol= 26,nrow=1786,dimnames=list(c(1:1829),names))
  
  for(i in 1:20){
    file <- paste(dir, "/", "subnets_k_", i,".tsv", sep="")
    subnets <- read.delim(file, header=F, quote= "\"")
    ncol.sub <- ncol(subnets)
    subnets <- subnets[which(subnets[,ncol.sub] < 0.05),]
    
    #delelte duplicated p-values
    dup <- duplicated(subnets[,ncol(subnets)])
    del <- which(dup == T)
    if(length(del) != 0) subnets <- subnets[-(del),]
    nrow.sub <- nrow(subnets)
    subnets <- subnets[order(subnets[,1]),]

    del <- c()
    c <- 1
    
    for(j in 1:(nrow.sub-1)){
#print(c("j",j))
      s1 <- as.numeric(subnets[j,1:(ncol.sub-1)])
      s2 <- as.numeric(subnets[j+1,1:(ncol.sub-1)])

      len.set <- length(intersect(s1,s2))
#print(c("len.set",len.set))
		# i zwischen 10 -30

	if(is.element(i,c(5:28))) min <- round(0.7 * i,0)

	  min <- round(0.8 * i,0)
		
#print(c("number_equal",number_equal))		
      if(is.element(len.set,c(min:i))){ #|| len.set == 2  || len.set == 3  || len.set == 4 ){
        idx <- c()
        idx[1] <- subnets[j,ncol.sub]
        idx[2] <- subnets[j+1,ncol.sub]
        idx <- which(idx == max(idx))
        if(idx == 1){
          del[c] <- j
        } else{
          del[c] <- j+1
        }
        c <- c+1
      }
      #res.mat[j,i-4] <- paste((V(G)$name[subnet.j]),collapse="_")
    }
    if(length(del) != 0) subnets <- subnets[-(del),]
    subnets <- subnets[order(subnets[,ncol(subnets)]),]
    
    write.table(subnets, file=paste(out.dir, "/", "final_subnets_k_",i,".tsv",sep=""), append = F, quote = TRUE, sep = "\t ",col.names=F, row.names=F)
    print(c("finished file: ", i))
    #res.mat[,i] <- unique(sort(res.mat[,i])) 
    
   # print(c("finished file: ", i))
  }
  #write.table(res.mat, file=paste(out.dir, "/", "subnet_genes2.tsv",sep=""), append = F, quote = TRUE, sep = "\t ",col.names=T, row.names=F)
  #return(res.mat)
}

#*********************************************************************************
# MAIN
#*********************************************************************************
#get.genes(final.G,in.dir,out.dir=out.d)



#categorie.C5 <- read.delim(categorie.file, header=T, quote= "\"")
#subnets <- read.delim(subnets.file, header=F, quote= "\"")
#len.subnet <- length(subnets[1,])-1
#subnet.i <- as.numeric(subnets[1,1:len.subnets])
#unlist(strsplit(levels(factor(categorie.C5[1, 4])), ","))
#unlist(strsplit(levels(factor(test[1,])),"_"))
