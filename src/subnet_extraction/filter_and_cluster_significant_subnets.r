#filter significant subnets and calculate log p.value to be able to compare the subnets
get.significant.subnets <- function(s,G,dir,quantile.dir){
    quantile.file <- paste(quantile.dir, "/", "all_quantile.tsv",sep="")
    quantile.alpha <- read.delim(quantile.file, header=F, quote= "\"")
    file <- paste(dir, "/", "subnets_k_", s,".tsv", sep="")
    subnets <- read.delim(file, header=F, quote= "\"")
    
    ncol.sub <- ncol(subnets)
    alpha <- quantile.alpha[1,s]
    subnets <- subnets[which(subnets[,ncol.sub] < alpha),1:ncol.sub]

    for(i in 1:nrow(subnets)){
        subnet <- as.numeric(subnets[i,1:s])
        p.values <- V(G)$p.value[subnet] 
        
        #get teststatistic X2 
        len.p <- length(p.values)
        X2 <- 0
        X2 <- sum((-2)*log(p.values))
        #print(c("3: X2: ",X2))
        
        # get new p-value
        df <- 2 * len.p
        p.value <- pchisq(X2,df,lower.tail = F,log.p=T)
       # print(c("4: get.p -> p.value",p.value))
        subnets[i,s+1] <- p.value
    }
    return(subnets)
}

calc_dist <- function(v1,v2){
	res <- 1 - (length(intersect(v1,v2))/length(union(v1,v2)))
	return(res)
}

get_distmat <- function(submat){
	all_row <- nrow(submat)
	res_mat <- matrix(1,all_row,all_row)
	
	for(col in 1:(all_row-1)){
		for(row in 1:(all_row - col)){
			
			#get rows and calculate distance
			res_mat[(row+col),col] <- calc_dist(as.numeric(submat[col,]),as.numeric(submat[(row+col),]))
		}
	}
	return(as.dist(res_mat,F,F))
}


get.genes.clustering <- function(s, G, dir, out.dir, quantile.dir, cut){

	 subnets <- get.significant.subnets(s,G,dir,quantile.dir)
   # quantile.file <- paste(quantile.dir, "/", "all_quantile.tsv",sep="")
   # quantile.alpha <- read.delim(quantile.file, header=F, quote= "\"")
   # file <- paste(dir, "/", "subnets_k_", s,".tsv", sep="")
   # subnets <- read.delim(file, header=F, quote= "\"")

   # subnets <- subnets[1:100,]
    ncol.sub <- ncol(subnets)
    
    #if(is.element(s,c(2:5,77:100))){
    #    subnets_new <- subnets[which(subnets[,ncol.sub] < 0.001),1:ncol.sub-1]
    #} else{
    #    alpha <- quantile.alpha[1,s]
    #    subnets_new <- subnets[which(subnets[,ncol.sub] < alpha),1:ncol.sub-1]
    #}
	
	#if(s == 1){
  #      result <- subnets[which(subnets[,ncol.sub] < 2.36630383341221e-06),1:ncol.sub]
  #  } else{
        
        #nrow.sub <- nrow(subnets)
        #subnets <- subnets[order(subnets[,1]),1:ncol.sub-1]
        d <- get_distmat(subnets[,1:ncol.sub-1])
        
        fit <- hclust(d, method="complete")
        
        d <- NULL
        # gc()
        
        pic <- paste(out.dir, "/dendogramm_all_k",s,".pdf", sep="")
        pdf(file=pic)
        plot(fit)
        dev.off()
        
        
        for(c in cut){
            print(c("cut:",c))
            groups <- cutree(fit, h=c)
            
            save(groups, file=paste("groups_k",s,"_",c,".Rdata",sep=""))
            
            max_group <- max(groups)
            
            all_idx <- 0
            
            for(g in 1:max_group){
                idx <- which(groups == g)
                sub <- subnets[idx,ncol.sub]
                minidx <- which(sub == min(sub))
                if(length(minidx) > 1){
                    minidx <- minidx[1]
                }
                all_idx[g] <- idx[minidx]
            }
            
            
            result <- subnets[all_idx,]
        
            
            write.table(result, file= paste(out.dir, "/","cut_",c,"_final_subnets_k_",s,".tsv",sep=""), append = F, quote = TRUE, sep = "\t ",col.names=F, row.names=F)
            
            if(nrow(result) > 3){
                
                #if(is.element(s,c(2:5,77:100))) alpha <- 0.001
                
                #cut_result <- result[which(result[,ncol.sub] < alpha),1:ncol.sub-1]
            
                cut_d <- get_distmat(result)
            
                cut_fit <- hclust(cut_d, method="complete")
            
            
                cut_pic <- paste(out.dir, "/dendogramm_",c,"_k",s,".pdf", sep="")
                pdf(file=cut_pic)
                plot(cut_fit)
                dev.off()
            }

            
            
            
            #groups <- NULL
    
            #result <- NULL
    #       all_idx <- NULL
    #       idx <- NULL
    #       minidx <- NULL
    #       s <- NULL
            
        }
       
   # }
    
    gc()
    #return(result)
}