#*******************************************************************************
# LIBRARIES
#*******************************************************************************
library("XML")
library("igraph")
library("gsubfn")

source("./filter_affy_data.R")
source("./build_network.R")
source("./greedy_new.R")
source("./init_sort_all_neighbors.R")
source("./pval_opt.R")
source("./get_categories.R")
source("./annotation.R")
source("./all_annotated_subnets.R")
#*******************************************************************************
# FILES
#*******************************************************************************

#interactome data
interactome.data <- "../../data/interactome/HI_2012_PRE.tsv"

#chip data original 
csv.file.ER.train <- "../../data/DEG_ER+_ER-/DEG_ER+_ER-_trainset.csv"

#chip data filtered
chip.data.file.ER.train <- "../../data/filtered_affy_data/ER+_vs_ER-_train_filtered.csv"

#GSEA annotation db
anno.f.name <- "msigdb_v4.0"
anno.dir.file  <- "../../data/annotation/GSEA"
#*********************************************************************************
# DIRECTORIES
#*********************************************************************************
#out.new.p <- "../data/greedy_new/submat/subnets"

#output directory of raw subnets data
#outdir.get.all <- "../data/greedy_new/submat/raw"

#output directory of filtered chip data
#filtered.file.out <- "../data/filtered.tsv"

#
genes2annotate <- "../../data/greedy_new/submat/genes_2_annotate/ER+_vs_ER-_trainset_genes2annotate"

#directory to final subnets ready for annotation or further analysis
#finalsubnets <- "../data/greedy_new//submat//subnets"

#directory to subnets which could be annotated
anno.subnets.out <- "../../data/annotation/anno_subnets"

#*********************************************************************************
# FILTER CHIP DATA (delete unique)
#*********************************************************************************
print("filter affymetrix data ...")
data.filtered <- filter.affy.data(csv.file.ER.train)
print(c("write filterd affymetrix data to file",chip.data.file.ER.train))
write.table(data.filtered, file = chip.data.file.ER.train, append = FALSE, quote = TRUE, sep = ",", row.names= F)

#*********************************************************************************
# BUILD NETWORK from interactome and chip data
#*********************************************************************************
print("build theorie graph ...")
G <- build.graph.theorie(interactome.data)

print("build final graph ...")

final.G <- build.final.network(G,chip.data.file.ER.train)

#*********************************************************************************
# get all subnets of length i - GREEDY 
#*********************************************************************************

#*********************************************************************************
# function get.all(final.G,i,outdir) 
# -> gets all possible raw-subnetworks of length i (correction of p.value)
# input:      G: graph with p-values mapped on the nodes, 
#             i: length of subnetwork, 
#        outdir: fileoutput directory
# output: data.frame with all raw subnetworks and a flag-column indicates weather 
# a subnetwork is min. (1), not min. (0) and network with no possible length of i (2)
#*********************************************************************************

#*********************************************************************************
# function get.p(final.G,n,k,dir)
# filter the raw-subnetworks (unique, only with flag = 1 (min p)) and 
# calculates the new p.value for the subnetwork
# input:      G: graph with p-values mapped on the nodes,
#             n: from network of length
#             k: to network of length
#           dir: directory of raw-subnetworks
#        outdir: output directory
# output: list 
#********************************************************************************

#outdir.raw files
output.init.greedy <- "../../data/greedy_new/submat/raw/ER+_vs_ER-_trainset_raw"

#outdir.pval
out.pval.greedy <- "../../data/greedy_new/submat/subnets/ER+_vs_ER-_trainset_subnets"

#print("get seed subnet ...")
get.initial.subnets(final.G,output.init.greedy)
get.p(final.G,1,4226,output.init.greedy,out.pval.greedy,method="greedy")

print("search for subnets...")
for(k in 2:100){
    seed_k <- paste(output.init.greedy, "/", "all_submat_k",k-1,".tsv",sep="")
    subnets_greedy(G= final.G,seed_k= seed_k, k= k, outdir.raw= output.init.greedy, outdir.pval= out.pval.greedy)
    seed_k <- NULL
    num.subs <- filter.raw(k,dir=output.init.greedy, outdir=out.pval.greedy, method="greedy")
    print(c("num.subs",num.subs))
    get.p(final.G,k,num.subs,output.init.greedy,out.pval.greedy,method="greedy")
    num.subs <- NULL
    print(c("finished subnets of length k=",k))
    gc()
}

#*********************************************************************************
# get all categories for annotation of subnetworks
#*********************************************************************************
anno.categories <- ana.gsea(anno.file=anno.f.name,anno.dir=anno.dir.file)

#*******************************************************************************
# GET GENES FOR ANNOTATION
#*******************************************************************************
#genes2annotate <- "../../data/greedy_new/submat/genes_2_annotate/ER+_vs_ER-_trainset_genes2annotate"

#print("get significant filtered genes ...")
#get.genes(G=final.G,dir=out.pval.greedy,out.dir=genes2annotate)


#*********************************************************************************
# gets genes for annotation (filters the final subnetwork file -> e.g delete duplicated p.values 
# to minimize number of subnetworks)
#*********************************************************************************
#print("get significant filtered genes ...")
#get.genes(G=final.G,dir=finalsubnets,out.dir=genes2annotate)

#for(f in 89:5){
# print(c("subnet of k= ",f))
# s.file <- paste(genes2annotate, "/", "final_subnets_k_", f,".tsv", sep="")
# get.fischer.pval(file= s.file, categories= anno.categories, chip.data= chip.data.file, alpha= 0.05, G= final.G, anno.file= anno.f.name, out.dir= anno.subnets.out)
#}
