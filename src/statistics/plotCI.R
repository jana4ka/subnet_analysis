#library('gplots')

library('plotrix')

dir.data <- "../data/greedy_new/submat/subnets/quantile"
data.file <- paste(dir.data, "/", "all_quantile_data.tsv",sep="")
data <- read.delim(data.file, header=F, quote= "\"")

quantile.dir <- "../data/greedy_new/random/subnets_random/subnets_full/quantile"

quantile.file <- paste(quantile.dir, "/", "all_quantile1.tsv",sep="")
quantile.alpha <- read.delim(quantile.file, header=F, quote= "\"")



x <- 1:50

#mean
y_random <- quantile.alpha[9,1:50]
y <- data[3,1:50]

#sd
s_random <- quantile.alpha[12,1:50]
s <- data[4,1:50]

#5%quantile
lb_random <- quantile.alpha[3,1:50]
lb <- data[1,1:50]

#95%quantile
ub_random <- quantile.alpha[6,1:50]
ub <- data[2,1:50]

y2_random = -log10(y_random)
lb2_random = -log10(lb_random)
ub2_random = -log10(ub_random)
s2_random = -log10(s_random)

y2 = -log10(y)
lb2 = -log10(lb)
ub2 = -log10(ub)
s2 = -log10(s)

#main="Significance assessment"

plotCI(x=x, y= y2_random, uiw=lb2_random,liw=ub2_random,lwd=1, col="red",gap=0.015, scol="blue",pch=20, cex=1,lty=1, ylab="-log10(p)", xlab="Size of subnetwork", ylim=c(2,165))
lines(x,y2_random,type="l",col="red",lwd=2)
#points(x,y2,type= "o",pch=20, col=3)
lines(x,y2,type="o",pch=20, col="darkgreen",lwd=2)
legend(1, 165, c("random data", "expression data"), col = c("red","darkgreen"),
text.col = "green4", lty = c(1, 1), pch = c(20, 20),
merge = TRUE, bg = "gray90")


x <- seq(-pi, pi, len = 65)
plot(x, sin(x), type = "l", ylim = c(-1.2, 1.8), col = 3, lty = 2)
points(x, cos(x), pch = 3, col = 4)
lines(x, tan(x), type = "b", lty = 1, pch = 4, col = 6)
title("legend(..., lty = c(2, -1, 1), pch = c(NA, 3, 4), merge = TRUE)",
cex.main = 1.1)
legend(-1, 1.9, c("sin", "cos", "tan"), col = c(3, 4, 6),
text.col = "green4", lty = c(2, -1, 1), pch = c(NA, 3, 4),
merge = TRUE, bg = "gray90")






plotCI(x=x, y= y2,uiw=lb2,liw=ub2,lwd=1, col="red",main="significance assessment",gap=0.015, scol="blue",pch=20, cex=1,lty=1, ylab="-log10(p)", xlab="size of subnetwork", add=TRUE)
lines(x,y2,type="l",col="red",lwd=3)

plot(x,y2_random, lwd=1, col="red",main="significance assessment",pch=20, cex=1,lty=1, ylab="-log10(p)", xlab="size of subnetwork", type="l")
lines(x,s_random,type="l",col="green",lwd=3)
plot(x,s_random, lwd=1, col="red",main="significance assessment",pch=20, cex=1,lty=1, ylab="-log10(p)", xlab="size of subnetwork", type="l")
lines(x,s_random,type="l",col="green",lwd=3)
