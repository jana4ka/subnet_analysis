stat_greedy_file <- "../data/greedy_new/submat/subnets/not_tracked/sig_statistics_greedy_09_12_13.tsv"
stat_greedy <- read.delim(stat_greedy_file, header=T, quote= "\"")

all <- as.numeric(stat_greedy[1,])
sig <- as.numeric(stat_greedy[2,])
uniq <- as.numeric(stat_greedy[3,])
filt <- as.numeric(stat_greedy[4,])

all_random_file <- "../data/greedy_new/random/subnets_random/random_k1_20_all_statistics_10_12_13.tsv"
all_random <- read.delim(all_random_file, header=T, quote= "\"")

sig_random_file <- "../data/greedy_new/random/subnets_random/random_k1_20_sig_statistics_10_12_13.tsv"
sig_random <- read.delim(sig_random_file, header=T, quote= "\"")

all_random <- as.numeric(all_random[1,])
sig_random <- as.numeric(sig_random[1,])

complete.file <- '../data/complete_new/submat_complete/subnets_complete/sig_statistics_complete_11_12_13.tsv'
complete <- read.delim(complete_file, header=T, quote= "\"")

all_complete <- as.numeric(complete[1,])
sig_complete <- as.numeric(complete[2,])



par(mfcol=c(1,2), mar=c(3,3,1,1), oma=c(1.5,2,1,1))
plot(sig,all,main="Subnetworks - greedy",xlab="Number of significant subnetworks",
	ylab="Number of subnetworks",pch=19,xlim = c(122,1544) , ylim = c(1417,2163))

plot(sig_random,all_random,main="Subnetworks - random",xlab="Number of significant subnetworks",
	ylab="Number of subnetworks",pch=19)

plot(sig_complete,all_complete,main="Subnetworks - complete",xlab="Number of significant subnetworks",
	ylab="Number of subnetworks",pch=5)


#plot(sig,all,main="Subnetworks - greedy - complete -random",xlab="Number of significant subnetworks",
#	ylab="Number of subnetworks",pch=19,col="blue",xlim=c(0,21000),ylim=(122,28000))
#par(new=T)
#plot(sig_random,all_random,xlab="Number of significant subnetworks",
#	ylab="Number of subnetworks",pch=2,col="green")
#par(new=T)
#plot(sig_complete,all_complete,xlab="Number of significant subnetworks",
#	ylab="Number of subnetworks",pch=5,col="yellow")

#plot(uniq,all,main="Subnetworks - greedy approach",xlab="#significant subnetworks (unique)",
#	ylab="Number of all subnetworks",pch=19)

#plot(uniq,sig,main="Subnetworks - greedy approach",xlab="#significant subnetworks",
#	ylab="#significant subnetworks (unique)",pch=19)

#plot(filt,all,main="Subnetworks - greedy approach",xlab="Number of significant subnetworks",
#	ylab="Number of all subnetworks",pch=19)
