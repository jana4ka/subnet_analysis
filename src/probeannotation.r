#*******************************************************************************************************
# Load packages and librarys
#*******************************************************************************************************
source("http://bioconductor.org/biocLite.R")
biocLite("illuminaHumanv3.db")
#biocLite("annotate")
#biocLite("affy")

#library(annotate)
#library(affy)
library(illuminaHumanv3.db)

#*******************************************************************************************************
# probeannotation
#*******************************************************************************************************
symbol <- illuminaHumanv3SYMBOL
genename <- illuminaHumanv3GENENAME

probe.annotation.illuminaHv3 <- function(identifier){
    
    # Get the probe identifiers that are mapped to a gene symbol/gene name
    mapped_probes <- mappedkeys(identifier)
    
    # Convert to a list
    identifier <- as.list(identifier[mapped_probes])
    
    return(identifier)
}

fill.matrix <- function(n, symbol, genename, result.mat){
    #sapply(1:n,function(i){})
    for(i in 1:n){
        id <- which(names(symbol) == probes[i])
        if(length(id) == 0){
            next
        } else{
            result.mat[i,"SYMBOL"] <- symbol[[id]]
            result.mat[i,"GENNAME"] <- genename[[id]]
        }
    }
    na.ids = which(result.mat[,"SYMBOL"] == "NA")
    result.mat <- result.mat[-na.ids,]
    
    return(result.mat)
}
