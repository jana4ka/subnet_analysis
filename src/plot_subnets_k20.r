library("igraph")

source("./build_network.R")

#interactome data
interactome.data <- "../../data/interactome/HI_2012_PRE.tsv"

#filtered chip data file
chip.data.file.ER.test <- "../../data/filtered_affy_data/ER+_vs_ER-_test_filtered.csv"

#*********************************************************************************
# BUILD NETWORK from interactome and chip data
#*********************************************************************************
print("build theorie graph ...")
G <- build.graph.theorie(interactome.data)

print("build final graph ...")
final.G <- build.final.network(G,chip.data.file.ER.test)

#*********************************************************************************
# Plot induced subnet 92 (length = 20 genes)
#*********************************************************************************
subnet.names.95 <-c("CARD9","CDC45","CDC5L","CEP55","DLGAP5","EIF4E2","GOLGA2","INPP5J",
"LMO4","MCM10","MCM6","MCM7","PARD6B","PDCD6IP","RCOR3","TENC1","TPX2","TTC25","UBXN11","ZNF417")
in.subgraph.95 <- induced.subgraph(final.G,subnet.names.95)

V(in.subgraph.95)$color <- "lightblue"
V(in.subgraph.95)$size <- 8
V(in.subgraph.95)$shape <- "circle"
V(in.subgraph.95)$label.color="black"
V(in.subgraph.95)$label.cex <- 0.7

#genes in list: "MCM6","MCM7","CDC45","DLGAP5","CEP55","TPX2","MCM10"
V(in.subgraph.95)[c(3,4,5,8,12,19,20)]$color <- "yellow"
V(in.subgraph.95)[c(3,4,5,8,12,19,20)]$shape <- "csquare"

pdf("../../data/pics/subnet_91_k20.pdf")
plot(in.subgraph.95,layout=layout.fruchterman.reingold,vertex.label.dist=0.5,main="Subnetwork 91 of length 20")
legend("bottomleft",
       legend = c('annotated genes','not annotated genes'),  
       col = c("yellow","lightblue"), 
       pch=c(15,16),
       cex = .9)
dev.off()

#*********************************************************************************
# Plot induced subnet 235 (length = 20 genes)
#*********************************************************************************
subnet.names.235 <- c("ABLIM3","CARD9","CDC45","CEP55","EFHC1","EIF4E2","IKZF3","MIPOL1",
"NR1H2","NR1H3","NRIP1","PDCD6IP","RARA","RARB","RCOR3","RXRG","SMAD3","TEKT4","WWP1","ZNF417")

in.subgraph.235 <- induced.subgraph(final.G,subnet.names.235)

V(in.subgraph.235)$color <- "lightblue"
V(in.subgraph.235)$size <- 5
V(in.subgraph.235)$shape <- "circle"
V(in.subgraph.235)$label.color="black"
V(in.subgraph.235)$label.cex <- 0.7

#genes in list: "SMAD3","RARA","RARB","RXRG","NRIP1","CDC45","WWP1","IKZF3","NR1H2","NR1H3"
V(in.subgraph.235)[c(1, 2, 3, 4, 5, 6, 9, 10, 18, 19)]$shape <- "csquare"
V(in.subgraph.235)[c(1, 2, 3, 4, 5, 6, 9, 10, 18, 19)]$color <- "yellow"

pdf("../../data/pics/subnet_235_k20.pdf")
plot(in.subgraph.235,layout=layout.fruchterman.reingold,vertex.label.dist=0.5,main="Subnetwork 235 of length 20")
legend("bottomleft",
       legend = c('annotated genes','not annotated genes'),  
       col = c("yellow","lightblue"), 
       pch=c(15,16),
       cex = .9)
dev.off()

#*********************************************************************************
# Plot induced subnet 272(length = 20 genes)
#*********************************************************************************
subnet.names.272 <- c("AGR3","AP2B1","CCDC148","CCDC153","CYHR1","IL6ST","KPNA2","KPNA3","KPNA4","MTA1",
"NME3","NUP50","NUP62","NUTF2","OIP5","RAPGEF3","SAT1","SERTAD3","SPG11","UBQLN1")

in.subgraph.272 <- induced.subgraph(final.G,subnet.names.272)

V(in.subgraph.272)$color <- "lightblue"
V(in.subgraph.272)$size <- 5
V(in.subgraph.272)$shape <- "circle"
V(in.subgraph.272)$label.color="black"
V(in.subgraph.272)$label.cex <- 0.7

#genes in list: "KPNA3","NUP50","NUP62","NUTF2","KPNA2","KPNA4","IL6ST","OIP5"
V(in.subgraph.272)[c(2, 3, 4, 5, 9, 10, 11, 12)]$shape <- "csquare"
V(in.subgraph.272)[c(2, 3, 4, 5, 9, 10, 11, 12)]$color <- "yellow"

pdf("../../data/pics/subnet_272_k20.pdf")
plot(in.subgraph.272,layout=layout.fruchterman.reingold,vertex.label.dist=0.5,main="Subnetwork 272 of length 20")
legend("bottomleft",
       legend = c('annotated genes','not annotated genes'),  
       col = c("yellow","lightblue"), 
       pch=c(15,16),
       cex = .9)
dev.off()

#*********************************************************************************
# Plot induced subnet 59(length = 16 genes)
#*********************************************************************************
subnet.names.59 <- c("BAD","BAK1","BCL11A","BCL2L1","BCL2L2","BIK","BMF","CARD9",
"EFHC1","EIF4E2","MEMO1","NCK2","PTGER3","REL","SEC14L4","ZNF417")

in.subgraph.59 <- induced.subgraph(final.G,subnet.names.59)

V(in.subgraph.59)$color <- "lightblue"
V(in.subgraph.59)$size <- 5
V(in.subgraph.59)$shape <- "circle"
V(in.subgraph.59)$label.color="black"
V(in.subgraph.59)$label.cex <- 0.7

#genes in list: "BAD","BAK1","BCL2L1","BIK","BCL2L2","BMF","PTGER3","CARD9","REL", "BCL11A","NCK2"
V(in.subgraph.59)[c(1,2,3,4,5,6,7,8,11,12,13)]$shape <- "csquare"
V(in.subgraph.59)[c(1,2,3,4,5,6,7,8,11,12,13)]$color <- "yellow"

pdf("../../data/pics/subnet_59_k16.pdf")
plot(in.subgraph.59,layout=layout.fruchterman.reingold,vertex.label.dist=0.5,main="Subnetwork 59 of length 16")
legend("bottomleft",
legend = c('annotated genes','not annotated genes'),
col = c("yellow","lightblue"),
pch=c(15,16),
cex = .9)
dev.off()
