#*******************************************************************************************************
# load packages and librarys
#*******************************************************************************************************
#source("http://bioconductor.org/biocLite.R")

library(affy)
library(survival)

source("./probeannotation.r")
#*******************************************************************************************************
# load expressionset and phenodata
#*******************************************************************************************************
setwd("../data/METABRIC_DATA/.R_OBJECTS_Expression_data")
load(file="Complete_METABRIC_Expression_Data.rbin")

metabric <- Complete_METABRIC_Expression_Data

#setwd("../.R_OBJECTS_OS")
#load(file="Complete_METABRIC_Clinical_Survival_Data_OS.rbin")

#setwd("../.R_OBJECTS_Clinical_data")
#load(file="Complete_METABRIC_Clinical_Survival_Data__DSS.rbin")

setwd("../.R_OBJECTS_Clinical_data")
load(file="Complete_METABRIC_Clinical_Features_Data.rbin")

setwd("../../../src")

pData(metabric) <- Complete_METABRIC_Clinical_Features_Data

OS <- read.table("../data/METABRIC_DATA/Clinical_Overall_Survival_Data_from_METABRIC.csv", header=T, stringsAsFactors = FALSE, sep=",")

#DFS <- read.table("../data/METABRIC_DATA/Complete_METABRIC_Clinical_Survival_Data__from_METABRIC.csv", header=T, stringsAsFactors = FALSE, sep=",")



#OS <- as.data.frame(OS_raw)

#OS.Surv <- Surv(OS_raw$time,OS_raw$status)

#*******************************************************************************************************
# Trainings- VS Validationset
#*******************************************************************************************************
# extracting ids from Expressionset
trainingsset.ids <- which(as.character(pData(metabric)$NOT_IN_OSLOVAL_IntClustMemb) != "NA") #length=995
validationset.ids <- which(is.na(pData(metabric)$NOT_IN_OSLOVAL_IntClustMemb)) #length=986

# extracting sampleNames for validation and trainingset
#training.names <- sampleNames(metabric)[trainingsset.ids]    #length=995
#validation.names <- sampleNames(metabric)[validationset.ids] #length=986

#seperate test- and trainingssets
OS.train <- OS[trainingsset.ids,]
OS.test <- OS[validationset.ids,]

#get expressionmatrices
exp.train <- exprs(metabric)[,trainingsset.ids]
exp.test <- exprs(metabric)[,validationset.ids]


#*******************************************************************************************************
#Extract expressionmatrix for trainings- and validationsets for ER- vs ER+
#*******************************************************************************************************
exprs.meta <- exprs(metabric)

#Expressionsets ER+, ER-
ER.pos.ids <- which(pData(metabric)$ER_IHC_status == "pos") # length=1505
ER.neg.ids <- which(pData(metabric)$ER_IHC_status == "neg") # length=435
#ER.pos <- exprs.meta[, ER.pos.ids]   # dim = 49576  1505
#ER.neg <- exprs.meta[, ER.neg.ids]   # dim = 49576   435

#Validationset ER-
set.ids.ER.neg.test <- intersect(validationset.ids,ER.neg.ids)
OS.test.ER.neg <- OS[set.ids.ER.neg.test,]
val.ER.neg.set <- exprs.meta[, set.ids.ER.neg.test] #dim(val.ER.neg.set) [1] 49576   240

#Validationset ER+
set.ids.ER.pos.test <- intersect(validationset.ids,ER.pos.ids)
OS.test.ER.pos <- OS[set.ids.ER.pos.test,]
val.ER.pos.set <- exprs.meta[, set.ids.ER.pos.test]
#val.ER.pos.set <- get.expressions(validationset.ids,ER.pos.ids) #dim(val.ER.neg.set) [1] 49576   705

#Trainingsset ER-
set.ids.ER.neg.train <- intersect(trainingsset.ids,ER.neg.ids)
OS.train.ER.neg <- OS[set.ids.ER.neg.train,]
train.ER.neg.set <- exprs.meta[, set.ids.ER.neg.train]
#train.ER.neg.set <- get.expressions(trainingsset.ids,ER.neg.ids) #dim(val.ER.neg.set) [1] 49576   195

#Trainingsset ER+
set.ids.ER.pos.train <- intersect(trainingsset.ids,ER.pos.ids)
OS.train.ER.pos <- OS[set.ids.ER.pos.train,]
train.ER.pos.set <- exprs.meta[, set.ids.ER.pos.train]
#train.ER.pos.set <- get.expressions(trainingsset.ids,ER.pos.ids) #dim(val.ER.neg.set) [1] 49576   800

#*******************************************************************************************************
#Coxph test
#*******************************************************************************************************
probes <- row.names(metabric)
n <- length(probes)

coxph.test <- function(n, probes, OS.data, exprs.mat){
    
    SYMBOL <- rep("NA",n)
    GENNAME <- rep("NA",n)
    p.logtest  <- rep("NA",n)
    p.waldtest <- rep("NA",n)
    p.sctest   <- rep("NA",n)
    
    result.mat <- matrix(c(probes, p.logtest, p.waldtest, p.sctest, SYMBOL, GENNAME),ncol=6,byrow=FALSE)
    colnames(result.mat) <-c("PROBES", "p-value logtest","p-value waldtest","p-value scoretest", "SYMBOL","GENNAME")
    
    for(i in 1:n){
        print(c("calculate coxph for gene", i, "of", n))
        cph.result <- coxph(Surv(OS.data$time,OS.data$status) ~ exprs.mat[i,])
        summcph <- summary(cph.result)
        
        #Likelihood ratio test
        result.mat[i,"p-value logtest"]  <- summcph$logtest["pvalue"]
        #Wald test
        result.mat[i,"p-value waldtest"] <- summcph$waldtest["pvalue"]
        #score test
        result.mat[i,"p-value scoretest"] <- summcph$sctest["pvalue"]
    }
    return(result.mat)
}

symbol <- probe.annotation.illuminaHv3(symbol)
genename <- probe.annotation.illuminaHv3(genename)

#*******************************************************************************************************
#get p-values, probeannotation and write tables -> testset+trainset OA-survival
#*******************************************************************************************************
resmat.test <- coxph.test(n, probes, OS.data= OS.test, exprs.mat= exp.test)
test.resmat.filtered <- fill.matrix(n, symbol, genename, result.mat= resmat.test)# nrow [1]29641
write.table(test.resmat.filtered, file = "../data/survival_coxph/OS/OS_validationset.csv",row.names=FALSE, sep=",")

resmat.test <- NULL
test.resmat.filtered <- NULL

resmat.train <- coxph.test(n, probes, OS.data= OS.train, exprs.mat= exp.train)
train.resmat.filtered <- fill.matrix(n, symbol, genename, result.mat= resmat.train)
write.table(train.resmat.filtered, file = "../data/survival_coxph/OS/OS_trainset.csv",row.names=FALSE, sep=",")

resmat.train <- NULL
train.resmat.filtered <- NULL
#*******************************************************************************************************
#get p-values, probeannotation and write tables -> testset OA-survival and ER.neg
#*******************************************************************************************************
resmat.test.ER.neg <- coxph.test(n, probes, OS.data= OS.test.ER.neg, exprs.mat= val.ER.neg.set) #
test.resmat.ER.neg.filtered <- fill.matrix(n, symbol, genename, result.mat=resmat.test.ER.neg)
write.table(test.resmat.ER.neg.filtered, file = "../data/survival_coxph/OS/OS_validationset_ER_neg.csv",row.names=FALSE, sep=",")

resmat.test.ER.neg <- NULL
test.resmat.ER.neg.filtered <- NULL

#*******************************************************************************************************
#get p-values, probeannotation and write tables -> trainset OA-survival and ER.neg
#*******************************************************************************************************
resmat.train.ER.neg <- coxph.test(n, probes, OS.data= OS.train.ER.neg, exprs.mat= train.ER.neg.set)#
train.resmat.ER.neg.filtered <- fill.matrix(n, symbol, genename, result.mat= resmat.train.ER.neg)
write.table(train.resmat.ER.neg.filtered, file = "../data/survival_coxph/OS/OS_trainset_ER_neg.csv",row.names=FALSE, sep=",")

resmat.train.ER.neg <- NULL
train.resmat.ER.neg.filtered <- NULL

#*******************************************************************************************************
#get p-values, probeannotation and write tables -> testset OA-survival and ER.pos
#*******************************************************************************************************
resmat.test.ER.pos <- coxph.test(n, probes, OS.data= OS.test.ER.pos, exprs.mat= val.ER.pos.set)
test.resmat.ER.pos.filtered <- fill.matrix(n, symbol, genename, result.mat= resmat.test.ER.pos)
write.table(test.resmat.ER.pos.filtered, file = "../data/survival_coxph/OS/OS_validationset_ER_pos.csv",row.names=FALSE, sep=",")

resmat.test.ER.pos <- NULL
test.resmat.ER.pos.filtered <- NULL

#*******************************************************************************************************
#get p-values, probeannotation and write tables -> trainset OA-survival and ER.pos
#*******************************************************************************************************
resmat.train.ER.pos <- coxph.test(n, probes, OS.data= OS.train.ER.pos, exprs.mat= train.ER.pos.set)
train.resmat.ER.pos.filtered <- fill.matrix(n, symbol, genename, result.mat= resmat.train.ER.pos)
write.table(train.resmat.ER.pos.filtered, file = "../data/survival_coxph/OS/OS_trainset_ER_pos.csv",row.names=FALSE, sep=",")

resmat.train.ER.pos <- NULL
train.resmat.ER.pos.filtered <- NULL
