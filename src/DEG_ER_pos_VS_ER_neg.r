#*******************************************************************************************************
# Load packages and librarys
#*******************************************************************************************************
#source("http://bioconductor.org/biocLite.R")

biocLite("illuminaHumanv3.db")
biocLite("annotate")
biocLite("affy")

library(annotate)
library(affy)
library(illuminaHumanv3.db)


#*******************************************************************************************************
# Load METABRIC expressionset and clinical phenodata
#*******************************************************************************************************
setwd("../data/METABRIC_DATA/.R_OBJECTS_Expression_data")
load(file="Complete_METABRIC_Expression_Data.rbin")

metabric <- Complete_METABRIC_Expression_Data
exprs.meta <- exprs(metabric)

setwd("../.R_OBJECTS_Clinical_data")
load(file="Complete_METABRIC_Clinical_Features_Data.rbin")

setwd("../../../src")
#phenodata <- read.table("../data/METABRIC_DATA/Complete_METABRIC_Clinical_Features_Data.csv", header=T, stringsAsFactors = FALSE, sep="\t")

pData(metabric) <- Complete_METABRIC_Clinical_Features_Data

trainingset <- read.table("../data/METABRIC_DATA/table_S43/IntClustFeatures_discoverydataset997.csv", header=T, stringsAsFactors = FALSE, sep="\t")

validationset <- read.table("../data/METABRIC_DATA/table_S43/IntClustFeatures_validationdataset995.csv", header=T, stringsAsFactors = FALSE, sep="\t")


#*******************************************************************************************************
# Observations
#*******************************************************************************************************

#dim(exprs(metabric))
#[1] 49576  1981

## Find the dimensions of the measured covariates
#dim(pData(metabric))
# [1] 1981   25

## Look at the names of the measured covariates
#names(pData(metabric))

# [1] "age_at_diagnosis"                         
# [2] "size"                                     
# [3] "lymph_nodes_positive"                     
# [4] "grade"                                    
# [5] "histological_type"                        
# [6] "ER_IHC_status"                            
# [7] "ER.Expr"                                  
# [8] "PR.Expr"                                  
# [9] "HER2_IHC_status"                          
#[10] "HER2_SNP6_state"                          
#[11] "Her2.Expr"                                
#[12] "Treatment"                                
#[13] "NOT_IN_OSLOVAL_menopausal_status_inferred"
#[14] "NOT_IN_OSLOVAL_group"                     
#[15] "NOT_IN_OSLOVAL_stage"                     
#[16] "NOT_IN_OSLOVAL_lymph_nodes_removed"       
#[17] "NOT_IN_OSLOVAL_NPI"                       
#[18] "NOT_IN_OSLOVAL_cellularity"               
#[19] "NOT_IN_OSLOVAL_P53_mutation_status"       
#[20] "NOT_IN_OSLOVAL_P53_mutation_type"         
#[21] "NOT_IN_OSLOVAL_P53_mutation_details"      
#[22] "NOT_IN_OSLOVAL_Pam50Subtype"              
#[23] "NOT_IN_OSLOVAL_IntClustMemb"              
#[24] "NOT_IN_OSLOVAL_Site"                      
#[25] "NOT_IN_OSLOVAL_Genefu"

## Make a table of the ER_IHC_status variable

#table(pData(metabric)$ER_IHC_status)

# neg  pos
# 435 1505

#*******************************************************************************************************
# Trainings- VS Validationset - Test
#*******************************************************************************************************
# extracting ids from Expressionset
validationset.ids <- which(as.character(pData(metabric)$NOT_IN_OSLOVAL_IntClustMemb) != "NA") #length=995
trainingsset.ids <- which(is.na(pData(metabric)$NOT_IN_OSLOVAL_IntClustMemb)) #length=986

# extracting sampleNames for validation and trainingset
training.names <- sampleNames(metabric)[trainingsset.ids]    #length=986
validation.names <- sampleNames(metabric)[validationset.ids] #length=995

# test
# extracting and transforming sampleNames for validation and trainingsset
patients.training <- colnames(trainingset)  #length = 997
patients.training <- chartr(".", "-", patients.training)
patients.validation <- colnames(validationset)  #length = 995
patients.validation <- chartr(".", "-", patients.validation)

#intersect(patients.training,training.names) #character(0)
#intersect(patients.validation,validation.names) #character(0)

#length(intersect(patients.validation,training.names)) # 986

# missing patients in dataset
#sort(setdiff(patients.validation,training.names))
#[1] "MB-0259" "MB-0281" "MB-0284" "MB-0403" "MB-3436" "MB-3582" "MB-3797"
#[8] "MB-5435" "MB-7082"

#intersect(patients.training,validation.names) # 995


#*******************************************************************************************************
#Extract expressionmatrix for trainings- and validationsets for ER- vs ER+
#*******************************************************************************************************
#Validation- and trainingssetids
val.ids <- trainingsset.ids # 986
train.ids <- validationset.ids # 995

#Expressionsets ER+, ER-
ER.pos.ids <- which(pData(metabric)$ER_IHC_status == "pos") # length=1505
ER.neg.ids <- which(pData(metabric)$ER_IHC_status == "neg") # length=435
ER.pos <- exprs.meta[, ER.pos.ids]   # dim = 49576  1505
ER.neg <- exprs.meta[, ER.neg.ids]   # dim = 49576   435


get.expressions <- function(data.ids,cov.ids){
    set.ids <- intersect(data.ids,cov.ids)
    set <- exprs.meta[, set.ids]
    
    return(set)
}

#Validationset ER-
val.ER.neg.set <- get.expressions(val.ids,ER.neg.ids) #dim(val.ER.neg.set) [1] 49576   240

#Validationset ER+
val.ER.pos.set <- get.expressions(val.ids,ER.pos.ids) #dim(val.ER.neg.set) [1] 49576   705

#Trainingsset ER-
train.ER.neg.set <- get.expressions(train.ids,ER.neg.ids) #dim(val.ER.neg.set) [1] 49576   195

#Trainingsset ER+
train.ER.pos.set <- get.expressions(train.ids,ER.pos.ids) #dim(val.ER.neg.set) [1] 49576   800

#*******************************************************************************************************
#DEG validationset
#*******************************************************************************************************
n <- nrow(exprs.meta)

get.p.values.ttest <- function(n, ER.neg.set, ER.pos.set){
    p.val <- sapply(1:n,function(i){t.test(ER.neg.set[i,],ER.pos.set[i,])$p.value})
    
    return(p.val)
}

p.validation <- get.p.values.ttest(n, val.ER.neg.set, val.ER.pos.set)   #sum(p.validation < 0.05) [1] 23982
p.training <- get.p.values.ttest(n, train.ER.neg.set, train.ER.pos.set) #sum(p.training < 0.05)   [1] 20416

probes <- row.names(metabric)

symmary.matrix <- function(n, p.values, probes, set){
    ##Bonferroni correction
    #p.bonf <- p.adjust(p.values,method="bonferroni")
    
    #p-value Benjamini-Hochberg correcton
    #p.bh <- p.adjust(p.values,method="BH")
    
    SYMBOL <- rep("NA",n)
    GENNAME <- rep("NA",n)
    
    DEG.mat <- matrix(c(probes, p.values, SYMBOL, GENNAME),ncol=4,byrow=FALSE)
    
    if(set == "validation"){
        colnames(DEG.mat) <-c("PROBES", "validationset p-value", "SYMBOL","GENNAME")
    }else{
        colnames(DEG.mat) <-c("PROBES", "trainingsset p-value", "SYMBOL","GENNAME")
    }
    return(DEG.mat)
}

DEG.mat.validation <- symmary.matrix(n, p.validation, probes, set="validation")
DEG.mat.training <- symmary.matrix(n, p.training, probes, set="training")

#*******************************************************************************************************
# probeannotation
#*******************************************************************************************************
symbol <- illuminaHumanv3SYMBOL
genename <- illuminaHumanv3GENENAME

probe.annotation.illuminaHv3 <- function(identifier){
    
    # Get the probe identifiers that are mapped to a gene symbol/gene name
    mapped_probes <- mappedkeys(identifier)
    
    # Convert to a list
    identifier <- as.list(identifier[mapped_probes])
    
    return(identifier)
}

symbol <- probe.annotation.illuminaHv3(symbol)
genename <- probe.annotation.illuminaHv3(genename)

fill.matrix <- function(n, symbol, genename, DEG.mat){
    #sapply(1:n,function(i){})
    for(i in 1:n){
        id <- which(names(symbol) == probes[i])
        if(length(id) == 0){
            next
        } else{
            DEG.mat[i,"SYMBOL"] <- symbol[[id]]
            DEG.mat[i,"GENNAME"] <- genename[[id]]
        }
    }
    na.ids = which(DEG.mat[,"SYMBOL"] == "NA")
    DEG.mat <- DEG.mat[-na.ids,]
    
    return(DEG.mat)
}

val.DEG.filtered <- fill.matrix(n, symbol, genename, DEG.mat.validation)
# nrow(val.DEG.filtered)    [1]29641
train.DEG.filtered <- fill.matrix(n, symbol, genename, DEG.mat.training)
# nrow(train.DEG.filtered)  [1] 29641

#dim(new.val.DEG) [1] 29641     6
write.table(val.DEG.filtered, file = "../data/DEG_ER+_ER-/DEG_ER+_ER-_validationset.csv",row.names=FALSE, sep=",")
write.table(train.DEG.filtered, file = "../data/DEG_ER+_ER-/DEG_ER+_ER-_trainingsset.csv",row.names=FALSE, sep=",")

#*******************************************************************************************************
# Plots
#*******************************************************************************************************
# ER_IHC_status neg
#jpeg("../data/pics/norm_hist_Metabric_er_neg.jpg")
#hist(validation.er.neg.set)
#dev.copy(postscript,"../data/pics/norm_hist_Metabric_er_neg.postscript")
#dev.off()
#dev.off()

#jpeg("../data/pics/norm_boxplot_Metabric_er_neg.jpg")
#boxplot(validation.er.neg.set)
#dev.copy(postscript,"../data/pics/norm_boxplot_Metabric_er_neg.postscript")
#dev.off()
#dev.off()

#*******************************************************************************************************
# ER_IHC_status pos
#jpeg("../data/pics/norm_hist_Metabric_er_pos.jpg")
#hist(validation.er.pos.set)
#dev.copy(postscript,"../data/pics/norm_hist_Metabric_er_pos.postscript")
#dev.off()
#dev.off()

#jpeg("../data/pics/norm_boxplot_Metabric_er_pos.jpg")
#boxplot(validation.er.pos.set)
#dev.copy(postscript,"../data/pics/norm_boxplot_Metabric_er_pos.postscript")
#dev.off()
#dev.off()
